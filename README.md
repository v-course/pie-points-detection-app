<!-- TOC --><a name="points-detection"></a>

# Points detection

<!-- TOC --><a name="exécution"></a>

<!-- TOC start (generated with https://github.com/derlin/bitdowntoc) -->

- [Points detection](#points-detection)
  - [Exécution](#exécution)
  - [Utilisation](#utilisation)
  - [Développement](#développement)
    - [In progress](#in-progress)
    - [TODO](#todo)

<!-- TOC end -->

## Exécution

1. La bibliothèque pywebview est nécessaire pour lancer l’application. Installer pywebview en suivant les instructions sur https://pywebview.flowrl.com/guide/installation.html.

2. Cloner le dépôt GitLab :

```
git clone https://gitlab.com/v-course/pie-points-detection-app.git
```

3. Aller dans le dossier racine :

```
cd pie-points-detection-app
```

4. Exécuter le script pour lancer l'application :

```
python3 ./start.py
```

<!-- TOC --><a name="utilisation"></a>

## Utilisation

1. Choisir une image.
2. Préciser les dimensions réelles souhaitées (pour les axes X et Y).
3. Changer le binary threshold s’il faut (le niveau de précision dans la détection des points, notamment important pour les images bruitées).
4. Appliquer les options.

![readme_instructions_1](readme_instructions_1.png)

5. Choisir un instrument Box select ou Lasso et sélectionner des points.
6. Choisir un angle et un groupe pour les points sélectionnés.
7. Appliquer les changements.
8. Télécharger le fichier CSV dès que tous les points ont un group et un angle.

![readme_instructions_2](readme_instructions_2.png)

<!-- TOC --><a name="développement"></a>

## Développement

Cette section contient la liste des tâches en progrès / des tâches à faire.

<!-- TOC --><a name="in-progress"></a>

### In progress

<!-- TOC --><a name="todo"></a>

### TODO

- ! Refactor code
- ! Split script js into several files
- Add possibility for a user to select a curve and change distance between points on this curve
- Add possibility for a user to add / remove points on a plot manually
- Add global error handler (show message to restart the app)
- Save CSV file to Donwloads default folder
