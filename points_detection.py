import numpy as np
import cv2
import pandas as pd


def sort_and_convert_to_dataframe(coordinates, headers):
    df = pd.DataFrame(coordinates, columns=["x", "y", "angle"])
    df["angle"] = df["angle"].astype(int)
    df["x"] = df["x"].astype(int)
    df["y"] = df["y"].astype(int)
    df = df.sort_values(["angle", "y", "x"], ascending=[True, False, False])
    # Reset indices after sorting
    df = df.reset_index(drop=True)
    df.columns = headers
    return df


def write_to_csv(coordinates_left, coordinates_right, filepath="coordinates.csv"):
    left_headers = ["COORD_X", "COORD_Y", "ANGLE"]
    left_df = sort_and_convert_to_dataframe(coordinates_left, left_headers)

    right_headers = ["COORD_X2", "COORD_Y2", "ANGLE2"]
    right_df = sort_and_convert_to_dataframe(coordinates_right, right_headers)

    df = pd.concat([left_df, right_df], axis=1)

    # Fix: for some reason after concat some of the columns become of type float
    for h in left_headers + right_headers:
        df[h] = df[h].astype("Int64")

    df.to_csv(filepath, index=False)
    return filepath


def scale_coordinates(coordinates, original_dimensions, desired_dimensions):
    desired_width, desired_height = desired_dimensions
    original_width, original_height = original_dimensions
    return list(
        map(
            lambda c: (
                int(c[0] / original_width * desired_width),
                int(c[1] / original_height * desired_height),
            ),
            coordinates,
        )
    )


def get_points_coordinates(image, real_width, real_height, binary_threshold=255 / 4):
    """
    Binary threshold is used to distinguish points from noise.
    Lower values may result in higher precision, but some noise may be detected as points as well.
    """
    image = cv2.flip(
        image, 0
    )  # Flipping so that the result coordinates correspond to the input image

    # Source: https://stackoverflow.com/questions/70300189/how-to-keep-only-black-color-text-in-the-image-using-opencv-python

    # Calculate channel K (black)
    image = image.astype(float) / 255.0
    black_channel = 1 - np.max(image, axis=2)
    black_channel = (255 * black_channel).astype(np.uint8)

    # Binarize image
    _, binary_image = cv2.threshold(
        black_channel, binary_threshold, 255, cv2.THRESH_BINARY
    )

    # Remove isolated pixels
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(binary_image, cv2.MORPH_OPEN, kernel)

    contours, _ = cv2.findContours(
        opening.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )

    points_coordinates = []

    for c in contours:
        # compute the center of the contour
        M = cv2.moments(c)
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        points_coordinates.append((cX, cY))

    height, width, _ = image.shape
    return scale_coordinates(
        points_coordinates,
        original_dimensions=(width, height),
        desired_dimensions=(real_width, real_height),
    )


def objective(x, a, b, c):
    return a * x + b * x**2 + c


# TODO
# Accepts points in format [[x0, y0], ..., [xn, yn]]
# Returns points of the same curve
# Increased density means more points returned (density = 1 will return the same points)
def polyfit(points, density=1, n=2):  # TODO
    return points
