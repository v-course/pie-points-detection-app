import webview
from api import Api

if __name__ == "__main__":
    api = Api()
    window = webview.create_window(
        "Points detection", "assets/index.html", js_api=api, min_size=(600, 450)
    )
    webview.start()
