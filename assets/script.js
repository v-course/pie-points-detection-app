const GROUP = { Left: 0, Right: 1 };

class Api {
  getPointsCoordinates(image, options) {
    const { scaleX, scaleY, binaryThreshold } = options;
    return this.getApi().getPointsCoordinates(
      image,
      scaleX,
      scaleY,
      binaryThreshold
    );
  }

  polyfit(points) {
    return this.getApi().polyfit(points);
  }

  exportCSV(coordinates_left, coordinates_right) {
    return this.getApi().exportCSV(coordinates_left, coordinates_right);
  }

  getApi() {
    const api = window?.pywebview?.api;
    if (api) {
      return api;
    }

    // For testing in browser
    return {
      getPointsCoordinates: () =>
        Promise.resolve([
          [100, 700],
          [200, 300],
          [450, 800],
        ]),
      exportCSV: () => Promise.resolve(),
    };
  }

  isApiDefined() {
    return window?.pywebview?.api;
  }
}

class ChartWrapper {
  GROUP_INDEX = 2;
  ANGLE_INDEX = 3;
  CURVE_INDEX = 4;
  CURVE_GROUP_INDEX = 1;

  // List of all points. Each point is represnted as a tuple [x, y, group, angle, curveId]
  points;

  // List of points selected by user.
  // Saved in format [[x,x,x...], [y,y,y...]] for easier processig.
  selectedPoints = [];

  // List of added curves
  // Saved in format [[color, group], ...]
  curveConfigs = [];

  getNumberOfPoints(group) {
    let n = 0;
    for (const p of this.points) {
      if (p[this.GROUP_INDEX] == group) {
        n++;
      }
    }
    return n;
  }

  getPointGroupConfig(group, lineColor = undefined) {
    return {
      type: "scatter",
      x: [],
      y: [],
      text: [],
      textposition: "top right",
      name: `Group ${group}`,
      mode: lineColor ? "lines+markers+text" : "markers+text",
      marker: {
        color: group === GROUP.Left ? "rgb(0, 110, 144)" : "rgb(241, 143, 1)",
      },
      line: { color: lineColor },
    };
  }

  getPlotConfig() {
    const pointsLeft = this.getPointGroupConfig(GROUP.Left);
    const pointsRight = this.getPointGroupConfig(GROUP.Right);

    const curves = this.curveConfigs.map(([color, group]) =>
      this.getPointGroupConfig(group, color)
    );

    let maxY = 0;

    this.points.forEach((p) => {
      if (p[1] > maxY) {
        maxY = p[1];
      }

      const pointGroup = p[this.GROUP_INDEX];
      const pointCurve = p[this.CURVE_INDEX];

      // Right group and point does not belong to any curve
      if (pointGroup == GROUP.Right && pointCurve == -1) {
        pointsRight.x.push(p[0]);
        pointsRight.y.push(p[1]);
        pointsRight.text.push(p[this.ANGLE_INDEX]);
      }
      // Left group and point does not belong to any curve
      else if (pointCurve == -1) {
        pointsLeft.x.push(p[0]);
        pointsLeft.y.push(p[1]);
        pointsLeft.text.push(p[this.ANGLE_INDEX]);
      }
      // Point belongs to some curve
      else {
        curves[pointCurve].x.push(p[0]);
        curves[pointCurve].y.push(p[1]);
        curves[pointCurve].text.push(p[this.ANGLE_INDEX]);
      }
    });

    let height = Math.min(1000, maxY);
    height = Math.max(600, height);

    const layout = {
      autosize: true,
      height,
      xaxis: {
        constrain: "domain",
      },
      yaxis: {
        scaleanchor: "x",
      },
      dragmode: "select",
    };

    return { data: [pointsLeft, pointsRight, ...curves], layout };
  }

  plotPoints(pointsCoordinates) {
    this.points = pointsCoordinates.map((p) => [...p, GROUP.Left, -1, -1]);
    this.plotCurrentPoints();
  }

  plotCurrentPoints() {
    const chartElement = this.getChartElement();
    const { data, layout } = this.getPlotConfig();
    Plotly.newPlot(chartElement, data, layout);

    chartElement.on("plotly_selected", (eventData) => {
      const x = eventData.points.map((p) => p.x);
      const y = eventData.points.map((p) => p.y);

      this.selectedPoints = [x, y];
    });
  }

  // Returns boolean value to indicate whether all points have a group and angle selected
  setGroupAndAngleForSelectedPoints(group, angle) {
    let allPointsReady = true;

    for (let i = 0; i < this.selectedPoints[0].length; i++) {
      const x = this.selectedPoints[0][i];
      const y = this.selectedPoints[1][i];

      for (let j = 0; j < this.points.length; j++) {
        const p = this.points[j];
        if (p[0] == x && p[1] == y) {
          this.points[j][this.ANGLE_INDEX] = angle;
          this.points[j][this.GROUP_INDEX] = group;
        }
      }
    }

    for (const p of this.points) {
      if (p[this.ANGLE_INDEX] == -1) {
        allPointsReady = false;
        break;
      }
    }

    this.purge();
    this.plotCurrentPoints();
    return allPointsReady;
  }

  purge() {
    const height = this.getChartElement().getBoundingClientRect().height;
    this.getChartElement().style.height = `${height}px`;
    Plotly.purge(this.getChartElement());
  }

  getChartElement() {
    return document.getElementById("chart");
  }

  // Returns color and index of the added curve
  addCurveForSelectedPoints() {
    const newCurveIndex = this.curveConfigs.length;
    const curveColor = this.getRandomColor();
    let curveGroup = -1;

    for (let i = 0; i < this.selectedPoints[0].length; i++) {
      const x = this.selectedPoints[0][i];
      const y = this.selectedPoints[1][i];

      for (let j = 0; j < this.points.length; j++) {
        const p = this.points[j];

        if (j == 0) {
          curveGroup = p[this.GROUP_INDEX];
        }

        if (p[0] == x && p[1] == y) {
          this.points[j][this.CURVE_INDEX] = newCurveIndex;
          // All points of the same curve should have one group
          this.points[j][this.GROUP_INDEX] = curveGroup;
        }
      }
    }

    this.curveConfigs.push([curveColor, curveGroup]);

    this.purge();
    this.plotCurrentPoints();
    return { color: curveColor, index: newCurveIndex };
  }

  addMorePoints(curveIndex) {
    const curvePoints = [];
    const group = this.curveConfigs[curveIndex][this.CURVE_GROUP_INDEX];
    const angle = -1;

    this.points = this.points.filter((p) => {
      if (p[this.CURVE_INDEX] == curveIndex) {
        curvePoints.push(p);
      }
      return p[this.CURVE_INDEX] != curveIndex;
    });

    API.polyfit(curvePoints, 1.25).then((ps) => {
      this.points.push(
        ...ps.map((p) => [p[0], p[1], group, angle, curveIndex])
      );
      this.purge();
      this.plotCurrentPoints();
    });
  }

  removePoints(curveIndex) {
    const curvePoints = [];
    const group = this.curveConfigs[curveIndex][this.CURVE_GROUP_INDEX];
    const angle = -1;

    this.points = this.points.filter((p) => {
      if (p[this.CURVE_INDEX] == curveIndex) {
        curvePoints.push(p);
      }
      return p[this.CURVE_INDEX] != curveIndex;
    });

    API.polyfit(curvePoints, 0.75).then((ps) => {
      this.points.push(
        ...ps.map((p) => [p[0], p[1], group, angle, curveIndex])
      );
      this.purge();
      this.plotCurrentPoints();
    });
  }

  getRandomColor() {
    const letters = "0123456789ABCDEF";
    let color = "#";
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
}

const API = new Api();
const CHART_WRAPPER = new ChartWrapper();

function showInitialOptionsError(text) {
  document.getElementById("errorMessage").innerText = text;
}

function clearInitialOptionsError() {
  document.getElementById("errorMessage").innerText = "";
}

function applyOptions() {
  const image = document.getElementById("inputImage");
  const src = image.src;

  if (
    document.getElementById("scaleX").value == "" ||
    document.getElementById("scaleY").value == ""
  ) {
    return showInitialOptionsError("X and Y scale should be provided");
  }

  if (!src || src.includes("no-image.jpg")) {
    return showInitialOptionsError("Image should be provided");
  }

  if (!API.isApiDefined()) {
    return showInitialOptionsError(
      "Internet connection required for loading the chart library"
    );
  }

  clearInitialOptionsError();

  const scaleX = parseFloat(document.getElementById("scaleX").value);
  const scaleY = parseFloat(document.getElementById("scaleY").value);
  const binaryThreshold = parseInt(
    document.getElementById("binaryThreshold").value
  );

  showChartLoader();

  result = API.getPointsCoordinates(src, { scaleX, scaleY, binaryThreshold })
    .then((p) => {
      clearCurveElements();
      CHART_WRAPPER.plotPoints(p);
      // Button is always disabled since this functionality is not ready
      // document.getElementById("addCurveButton").disabled = false;
    })
    .catch((e) => (document.getElementById("error").innerText = e))
    .finally(() => hideChartLoader());
}

function showChartLoader() {
  document.getElementById("loadingLabel").style.visibility = "visible";
}

function hideChartLoader() {
  document.getElementById("loadingLabel").style.visibility = "hidden";
}

function setInput(input) {
  const fReader = new FileReader();

  fReader.readAsDataURL(input.files[0]);

  fReader.onloadend = (event) => {
    const image = document.getElementById("inputImage");
    image.src = event.target.result;
  };
}

function exportCSV() {
  const pointsToExportLeft = [];
  const pointsToExportRight = [];

  for (const p of CHART_WRAPPER.points) {
    if (p[CHART_WRAPPER.GROUP_INDEX] == GROUP.Left) {
      pointsToExportLeft.push([p[0], p[1], p[CHART_WRAPPER.ANGLE_INDEX]]);
    } else if (p[CHART_WRAPPER.GROUP_INDEX] == GROUP.Right) {
      pointsToExportRight.push([p[0], p[1], p[CHART_WRAPPER.ANGLE_INDEX]]);
    }
  }

  API.exportCSV(pointsToExportLeft, pointsToExportRight).then((filepath) => {
    const message = document.getElementById("exportCSVMessage");
    message.innerText = `The coordinates has been saved to ${filepath} (root folder)`;
    setTimeout(() => (message.innerText = ""), 10_000);
  });
}

let curveElements = [];

function clearCurveElements() {
  for (const curve of curveElements) {
    curve.remove();
  }
  curveElements = [];
}

function addCurveForSelectedPoints() {
  const config = CHART_WRAPPER.addCurveForSelectedPoints();
  const element = getCurveElement(config);
  document.getElementById("curves").appendChild(element);
  curveElements.push(element);
}

function getCurveElement(config) {
  const element = document.createElement("div");
  Object.assign(element.style, {
    display: "flex",
    gap: "10px",
    "align-items": "center",
  });

  const label = document.createElement("p");
  label.innerText = `Curve #${config.index}`;
  label.style.color = config.color;
  element.appendChild(label);

  const increaseButton = document.createElement("button");
  increaseButton.innerText = "↑";
  increaseButton.classList.add("button");
  Object.assign(increaseButton.style, {
    color: config.color,
    "border-color": config.color,
  });
  increaseButton.onclick = () => CHART_WRAPPER.addMorePoints(config.index);
  element.appendChild(increaseButton);

  const decreaseButton = document.createElement("button");
  decreaseButton.innerText = "↓";
  decreaseButton.classList.add("button");
  Object.assign(decreaseButton.style, {
    color: config.color,
    "border-color": config.color,
  });
  decreaseButton.onclick = () => CHART_WRAPPER.removePoints(config.index);
  element.appendChild(decreaseButton);

  return element;
}

function setGroupAndAngleForSelectedPoints() {
  document.getElementById("plotError").innerText = "";
  const data = Object.fromEntries(new FormData(form));
  const allPointsReady = CHART_WRAPPER.setGroupAndAngleForSelectedPoints(
    data.group,
    data.angle
  );
  if (allPointsReady) {
    // show warning if groups have unpair number of elements
    if (
      CHART_WRAPPER.getNumberOfPoints(GROUP.Left) % 2 == 1 ||
      CHART_WRAPPER.getNumberOfPoints(GROUP.Right) % 2 == 1
    ) {
      document.getElementById("plotError").innerText =
        "Warning: there must be an even number of points in each group.";
    }
    document.getElementById("exportCSVButton").disabled = false;
  }
}

const form = document.getElementById("groupAndAngle");
form.addEventListener("submit", (e) => e.preventDefault());
form.addEventListener("change", (e) => {
  e.preventDefault();
  const data = Object.fromEntries(new FormData(form));

  if (data.group !== undefined && data.angle !== undefined) {
    document.getElementById("submitGroupAngleButton").disabled = false;
  }
});
