import cv2
import base64
import io
from imageio.v3 import imread
from points_detection import get_points_coordinates, write_to_csv, polyfit
import webview


class Api:
    def getPointsCoordinates(self, image_path, scale_x, scale_y, binary_threshold):
        header, encoded = image_path.split(",", 1)
        image_data = base64.b64decode(encoded)
        image = imread(io.BytesIO(image_data))
        cv2_image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        height, width, _ = image.shape
        points_coordinates = get_points_coordinates(
            cv2_image,
            real_width=scale_x,
            real_height=scale_y,
            binary_threshold=binary_threshold,
        )
        return points_coordinates

    def exportCSV(self, coordinates_left, coordinates_right):
        """
        Format of coordinates_left and coordinates_right: list of tuples (coordinate_x, coordinate_y, angle)
        Angle stands for orientation and is selected by user. Possible values:
        - Gauche 0
        - Droite 1
        - Haut 2
        - Bas 3
        coordinates_left and coordinates_right will be connected together in the final artwork.
        """
        return write_to_csv(coordinates_left, coordinates_right)

    def toggleFullscreen(self):
        webview.windows[0].toggle_fullscreen()

    def polyfit(self, points, density=1):
        return polyfit(points, density)
